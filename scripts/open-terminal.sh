#!/bin/bash
i3-msg 'workspace Terminal'
 
window_exists=$(i3-msg -t get_tree | jq -r 'recurse(.nodes[]) | select(.name!=null and (.name | contains("Alacritty")))| any(recurse(.nodes[]); .window_properties.class == "Alacritty")')


if [[ "$window_exists" =~ "true" ]]; then
    exit 0
else
	i3-msg 'exec alacritty'
fi
