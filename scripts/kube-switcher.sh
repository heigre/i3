#!/bin/bash
# kube-switcher.sh

# Define ASCII art.
ascii_art="KUBE SWITCHER"

# Print ASCII art and newlines to push it to the top of the screen.
echo "$ascii_art"

clear -x
# Run fzf to select a context.
context=$(kubectl config get-contexts -o name | fzf --prompt "Select KubeContext > ")

# Switch to the selected context if one was chosen.
if [ $? -eq 0 ]; then
    if [[ $context ]]; then
        kubectl config use-context "$context"
    fi
else
    echo 'Exited without selection'
fi
