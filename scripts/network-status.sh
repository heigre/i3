if [ "$BLOCK_BUTTON" = "1" ]; then 
	alacritty --class="fullscreen" -e nmtui  
else 
    # Check if network resolves within 1 second  
    NETWORK_STATUS=$(ping -c 1 1.1.1.1 | grep "time=")
    
    # Set color and icon based on network status
    if [ -n "$NETWORK_STATUS" ]; then
        ICON="@"
        COLOR="#00FF00" # Green for connected
    else
        ICON="#" 
    fi
    echo "<span color='$COLOR'>$ICON</span>"
fi
