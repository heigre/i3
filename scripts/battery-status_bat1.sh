#!/bin/bash
if [ $HOSTNAME = "iridium" ]; then
    # Get battery percentage
    PERCENTAGE=$(cat /sys/class/power_supply/BAT1/capacity)
    
    # Get charging status
    STATUS=$(cat /sys/class/power_supply/BAT0/status)
    
    # Set color and icon based on battery status
    if [ "$STATUS" = "Discharging" ]; then
        if [ "$PERCENTAGE" -lt 10 ]; then
    		ICON="  "
            COLOR="#FF0000" # Red
        elif [ "$PERCENTAGE" -lt 25 ]; then
    		ICON="  "
            COLOR="#FF0000" # Red
        elif [ "$PERCENTAGE" -lt 50 ]; then
    		ICON="  "
            COLOR="#FFFF00" # Yellow
    	elif [ "$PERCENTAGE" -lt 75 ]; then
    		ICON="  "
            COLOR="#00FF00" # Green
    	else
    		ICON="  "
            COLOR="#00FF00" # Green
        fi
    else
        ICON="⚡"
        COLOR="#00FF00" # Green
    fi
    
    # Output
    echo "<span color='$COLOR'>"EXT:" $ICON $PERCENTAGE%</span>"
else
    echo ""
fi
