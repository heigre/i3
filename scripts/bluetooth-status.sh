if [ "$BLOCK_BUTTON" = "1" ]; then 
    alacritty --class="fullscreen" -e bluetuith
else 
    # Check if Bluetooth is powered on
    POWERED_STATUS=$(bluetoothctl show | grep "Powered: yes")
    
    # Check if any Bluetooth device is connected
    CONNECTED_STATUS=$(bluetoothctl info | grep "Connected: yes")
    
    # Set color and icon based on Bluetooth status
    if [ -n "$POWERED_STATUS" ]; then
        if [ -n "$CONNECTED_STATUS" ]; then
            ICON="󰂯"
            COLOR="#00FF00" # Green for connected
        else
            ICON="󰂯" 
            COLOR="#FF0000" # Red for disconnected
        fi
    else
        ICON="󰂲" 
    fi
    echo "<span color='$COLOR'>$ICON</span>"
fi
