#!/bin/bash
i3-msg 'workspace  MS Teams'
 
window_exists=$(i3-msg -t get_tree | jq -r 'recurse(.nodes[]) | select(.name | contains("Teams"))| any(recurse(.nodes[]); .window_properties.class == "Teams")')

if [[ "$window_exists" =~ "true" ]]; then
    exit 0
else
    i3-msg 'exec teams'
fi
