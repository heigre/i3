#!/bin/bash
i3-msg 'workspace  Firefox'

# Check if application is already open  
window_exists=$(i3-msg -t get_tree | jq -r 'recurse(.nodes[]) | select(.name!=null)| .name' | grep "Firefox")

if [[ ! -z "$window_exists" ]]; then
    echo "Firefox is already open"
    exit 0
else
    #i3-msg 'exec firefox'
fi
