#!/bin/bash

# Lock the screen with dark grey background
i3lock -c 2e3440 -n 

# Wait for the lock to open
while pgrep -x i3lock > /dev/null; do sleep 1; done

# Reactivate lang toggle and picom
exec "setxkbmap -layout us,no -option 'grp:ctrl_space_toggle'"
exec picom --daemon --config ~/.config/picom/picom.conf


