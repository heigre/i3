#!/bin/bash

# Find which computer you are using based on hostname
# Known hostnames:
# radium - Desktop machine at home
# radon - Laptop at work
# iridium - Private laptop


if [ "$HOSTNAME" = "radium" ]; then
    RADIUM_MAIN_MONITOR="DP-0"
    RADIUM_SECONDARY_MONITOR="DP-2"
    RADIUM_SINGLE_MONITOR_PROFILE="~/.screenlayout/home-single-screen.sh"
    RADIUM_MULTI_MONITOR_PROFILE="~/.screenlayout/home-multi-screen.sh"
elif [ "$HOSTNAME" = "silicon" ]; then
    SILICON_INTERNAL_OUTPUT="eDP-1"
    SILICON_THINKVISION_OUTPUT="DP-1-0"
    SILICON_INTERNAL_OUTPUT_PROFILE="~/.screenlayout/laptop-single-screen.sh"
    SILICON_THINKVISION_OUTPUT_PROFILE="~/.screenlayout/thinkvision.sh"
    SILICON_YEALINK_OUTPUT="DP-1-2"
elif [ "$HOSTNAME" = "iridium" ]; then
    IRIDIUM_MAIN_MONITOR="eDP-1"
    #IRIDIUM_SECONDARY_MONITOR="DP-2"
    IRIDIUM_SINGLE_MONITOR_PROFILE="~/.screenlayout/laptop-onboard-screen.sh"
    #IRIDIUM_MULTI_MONITOR_PROFILE="~/.screenlayout/home-multi-screen.sh"
else
    echo "Unknown hostname: $HOSTNAME"
    exit 1
fi


if [ "$BLOCK_BUTTON" = "1" ]; then
    arandr &
else
    if [ "$HOSTNAME" = "radium" ]; then
        echo "Host: $HOSTNAME"
        # Check if the second monitor is connected
        if xrandr | grep -q "$RADIUM_SECONDARY_MONITOR connected"; then
            echo "Second monitor connected"
            CURRENT_PROFILE="Radium Multi Screen"
            exec $RADIUM_MULTI_MONITOR_PROFILE
            echo "󰍺  $CURRENT_PROFILE"
            exec --no-startup-id nitrogen --restore
       else
            echo "Main monitor only, no second monitor"
            CURRENT_PROFILE="Radium Single Screen"
            #arandr -output $RADIUM_SINGLE_MONITOR_PROFILE
            exec $RADIUM_SINGLE_MONITOR_PROFILE
            echo "󰍺  $CURRENT_PROFILE"
            exec --no-startup-id nitrogen --restore
        fi
    elif [ "$HOSTNAME" = "silicon" ]; then
        echo "Host: $HOSTNAME"
        # Check if any external monitors are connected
        if xrandr | grep -q "$SILICON_THINKVISION_OUTPUT connected"; then
            echo "ThinkVision connected"
            CURRENT_PROFILE="ThinkVision"
            exec $SILICON_THINKVISION_OUTPUT_PROFILE
            echo "󰍺  $CURRENT_PROFILE"
            exec --no-startup-id nitrogen --restore
        elif xrandr | grep -q "$SILICON_YEALINK_OUTPUT connected"; then
            echo "Yealink connected"
            CURRENT_PROFILE="Yealink"
            exec $SILICON_THINKVISION_OUTPUT_PROFILE
            echo "󰍺  $CURRENT_PROFILE"
            exec --no-startup-id nitrogen --restore
        else
            echo "Internal display only, no external monitor"
            CURRENT_PROFILE="Radon"
            exec $SILICON_INTERNAL_OUTPUT_PROFILE
            echo "󰍺  $CURRENT_PROFILE"
            exec --no-startup-id nitrogen --restore
        fi
    elif [ "$HOSTNAME" = "iridium" ]; then
        echo "Host: $HOSTNAME"
        CURRENT_PROFILE="Iridium Single Screen"
        exec $IRIDIUM_SINGLE_MONITOR_PROFILE
        echo "󰍺  $CURRENT_PROFILE"
        exec --no-startup-id nitrogen --restore
    else
        echo "Unknown hostname: $HOSTNAME"
        exit 1
    fi
fi
 
