#!/bin/bash
i3-msg 'workspace  Yazi'
 
window_exists=$(i3-msg -t get_tree | jq -r 'recurse(.nodes[]) | select(.name!=null and (.name | contains("Yazi")))| any(recurse(.nodes[]); .window_properties.class == "Yazi")' 2>/dev/null)

if [[ "$window_exists" != null ]]; then
    exit 0
else
    i3-msg 'exec alacritty -e yazi'
fi
