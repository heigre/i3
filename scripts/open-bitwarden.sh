#!/bin/bash
i3-msg 'workspace  Bitwarden'
 
window_exists=$(i3-msg -t get_tree | jq -r 'recurse(.nodes[]) | select(.name | contains("Bitwarden"))| any(recurse(.nodes[]); .window_properties.class == "Bitwarden")')

if [[ "$window_exists" =~ "true" ]]; then
    exit 0
else
    i3-msg 'exec bitwarden-desktop'
fi
