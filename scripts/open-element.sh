#!/bin/bash
i3-msg 'workspace  Element'
 
window_exists=$(i3-msg -t get_tree | jq -r 'recurse(.nodes[]) | select(.name | contains("Element"))| any(recurse(.nodes[]); .window_properties.class == "Element")')

if [[ "$window_exists" =~ "true" ]]; then
    exit 0
else
    i3-msg 'exec element-desktop'
fi
