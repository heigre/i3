#!/bin/bash

# Get battery percentage
PERCENTAGE_INT=$(cat /sys/class/power_supply/BAT0/capacity)
STATUS_INT=$(cat /sys/class/power_supply/BAT0/status)
if [ $HOSTNAME = "iridium" ]; then
    PERCENTAGE_EXT=$(cat /sys/class/power_supply/BAT1/capacity)
fi 

# Set color and icon based on battery status
if [ "$STATUS" = "Discharging" ]; then
    if [ "$PERCENTAGE" -lt 10 ]; then
		ICON="  "
        COLOR="#FF0000" # Red
    elif [ "$PERCENTAGE" -lt 25 ]; then
		ICON="  "
        COLOR="#FF0000" # Red
    elif [ "$PERCENTAGE" -lt 50 ]; then
		ICON="  "
        COLOR="#FFFF00" # Yellow
	elif [ "$PERCENTAGE" -lt 75 ]; then
		ICON="  "
        COLOR="#00FF00" # Green
	else
		ICON="  "
        COLOR="#00FF00" # Green
    fi
else
    ICON="⚡"
    COLOR="#00FF00" # Green
fi

# Output
echo "<span color='$COLOR'>"INT:" $ICON $PERCENTAGE%</span>"

# Turn computer off if battery is less than 5%
if [ "$STATUS" = "Discharging" ] && [ "$PERCENTAGE" -lt 5 ]; then
    shutdown -h now 
fi

#!/bin/bash
   
    # Set color and icon based on battery status
    if [ "$STATUS" = "Discharging" ]; then
        if [ "$PERCENTAGE" -lt 10 ]; then
    		ICON="  "
            COLOR="#FF0000" # Red
        elif [ "$PERCENTAGE" -lt 25 ]; then
    		ICON="  "
            COLOR="#FF0000" # Red
        elif [ "$PERCENTAGE" -lt 50 ]; then
    		ICON="  "
            COLOR="#FFFF00" # Yellow
    	elif [ "$PERCENTAGE" -lt 75 ]; then
    		ICON="  "
            COLOR="#00FF00" # Green
    	else
    		ICON="  "
            COLOR="#00FF00" # Green
        fi
    else
        ICON="⚡"
        COLOR="#00FF00" # Green
    fi
    
    # Output
    echo "<span color='$COLOR'>"EXT:" $ICON $PERCENTAGE%</span>"
else
    echo ""
fi
