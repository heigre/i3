#!/bin/bash
# GPT location: gpt4all/bin/chat
i3-msg 'workspace  AI'
 
window_exists=$(i3-msg -t get_tree | jq -r 'recurse(.nodes[]) | select(.name | contains("GPT"))| any(recurse(.nodes[]); .window_properties.class == "GPT")')

if [[ "$window_exists" =~ "true" ]]; then
    exit 0
else
    i3-msg 'exec gpt4all/bin/chat'
fi
