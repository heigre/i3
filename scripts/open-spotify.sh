#!/bin/bash
i3-msg 'workspace  Spotify'
 
window_exists=$(i3-msg -t get_tree | jq -r 'recurse(.nodes[]) | select(.name | contains("Spotify"))| any(recurse(.nodes[]); .window_properties.class == "Spotify")')

if [[ "$window_exists" =~ "true" ]]; then
    exit 0
else
    i3-msg 'exec spotify'
fi
