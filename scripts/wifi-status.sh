#!/bin/bash


if [ "$BLOCK_BUTTON" = "1" ]; then 
	alacritty --class="fullscreen" -e nmtui  
else 
	INTERFACE=$(iw dev | grep Interface | awk '{print $2}')
	SSID=$(iw "$INTERFACE" link | grep 'SSID:' | sed 's/SSID: //')
	SIGNAL=$(iw "$INTERFACE" link | grep 'signal:' | awk '{print $2}' | sed 's/-//')

	# Check for Internet connection
	ping -c 1 8.8.8.8 &> /dev/null

	if [ $? -ne 0 ]; then
		echo " 󰤫 No Internet"
		exit 0
	fi

	# Check for signal strength and provide icons accordingly
	if [ "$SSID" = "" ]; then
		echo " 󰤫 Disconnected "
	elif [ "$SIGNAL" -lt 60 ]; then
		echo " 󰤨 $SSID -$SIGNAL dBm "
	elif [ "$SIGNAL" -lt 70 ]; then
		echo " 󰤢 $SSID -$SIGNAL dBm "
	elif [ "$SIGNAL" -lt 80 ]; then
		echo " 󰤟 $SSID -$SIGNAL dBm "
	else
		echo " 󰤯 $SSID -$SIGNAL dBm "
	fi
fi
